<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca">
<context>
    <name>AboutPage</name>
    <message>
        <source>Change toolbar</source>
        <translation type="vanished">Canvia la barra d&apos;eines</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="29"/>
        <source>Draw freehand line</source>
        <translation>Dibuixa una línia a mà alçada</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="30"/>
        <source>Eraser</source>
        <translation>Goma d&apos;esborrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Sprayer</source>
        <translation>Esprai</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>Draw geometric shape</source>
        <translation>Dibuixa una forma geomètrica</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>Change color, width</source>
        <translation>Canvia el color i l&apos;amplada</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Geometrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Draw line</source>
        <translation>Dibuixa una línia</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>Draw rectangle</source>
        <translation>Dibuixa un rectangle</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Draw filled rectangle</source>
        <translation>Dibuixa un rectangle sòlid.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Draw square</source>
        <translation>Dibuixa un quadrat</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <source>Draw filled square</source>
        <translation>Dibuixa un quadrat sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Draw circle</source>
        <translation>Dibuixa un cercle</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>Draw filled circle</source>
        <translation>Dibuixa un cercle sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>Draw ellipse</source>
        <translation>Dibuixa una el·lipse</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>Draw filled ellipse</source>
        <translation>Dibuixa una el·lipse sòlida</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>Draw equilateral triangle</source>
        <translation>Dibuixa un triangle equilàter</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>Draw filled e-triangle</source>
        <translation>Dibuixa un triangle-eq sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Draw right isosceles triangle</source>
        <translation>Dibuixa un triangle isòsceles dret</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Draw filled ri-triangle</source>
        <translation>Dibuixa un triangle-isdr sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>Draw regular polygon</source>
        <translation>Dibuixa un polígon regular</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Draw filled regular polygon</source>
        <translation>Dibuixa un polígon regular sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Draw regular polygram</source>
        <translation>Dibuixa un polígram regular</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>Draw filled regular polygram</source>
        <translation>Dibuixa un polígram regular sòlid</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>More tools toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Add/cancel text</source>
        <translation>Afegeix/cancel·la un text</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Add/cancel image</source>
        <translation>Afegeix/cancel·la una imatge</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Draw thick arrow</source>
        <translation>Dibuixa una fletxa gruixuda</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>Drawing toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Draw filled thick arrow</source>
        <translation>Dibuixa una fletxa gruixuda sòlida</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Fill mode toggle</source>
        <translation>Commutador del mode de farciment</translation>
    </message>
    <message>
        <source>Text tool (cancel text)</source>
        <translation type="vanished">Eina de text (cancel·la text)</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Accept current text/image</source>
        <translation>Confirma la imatge/text actual</translation>
    </message>
    <message>
        <source>Accept current text</source>
        <translation type="vanished">Confirma el text actual</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Dimensioning tool</source>
        <translation>Eina de dimensionament</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>Grid</source>
        <translation>Reixeta</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>File toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>About Paint</source>
        <translation>Quant a Paint</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>Change settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="64"/>
        <source>Clear drawing</source>
        <translation>Neteja el dibuix</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="65"/>
        <source>Change background</source>
        <translation>Canvia el fons</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Save snapshot</source>
        <translation>Desa una captura instantània</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>Clipboard and layers toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="69"/>
        <source>Paste from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="70"/>
        <source>Accept current paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="71"/>
        <source>Layer down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>Layer up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>Configure layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>Shader toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="75"/>
        <source>Shader mode enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="76"/>
        <source>Accept current shading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Apply shader fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="78"/>
        <source>Show shader popup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>Configure shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <source>About </source>
        <translation>Quant a</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="137"/>
        <source>Version: </source>
        <translation>Versió:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="146"/>
        <source>translation credit placeholder</source>
        <translation>Traducció Agustí Clara</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="156"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
</context>
<context>
    <name>ColorSelector</name>
    <message>
        <location filename="../qml/components/ColorSelector.qml" line="27"/>
        <source>Edit color %1</source>
        <translation>Edita el color %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="23"/>
        <source>Paint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaSelector</name>
    <message>
        <source>Image selected</source>
        <translation type="vanished">Imatge seleccionada</translation>
    </message>
    <message>
        <source>Select image</source>
        <translation type="vanished">Seleccioneu una imatge</translation>
    </message>
</context>
<context>
    <name>Paint</name>
    <message>
        <location filename="../qml/pages/Paint.qml" line="48"/>
        <source>Save failed...</source>
        <translation type="unfinished">No s&apos;ha pogut desar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="277"/>
        <source>Swipe to change toolbar</source>
        <translation>Feu lliscar per canviar la barra d&apos;eines</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="297"/>
        <source>Pinch to zoom</source>
        <translation>Pinceu per fer zoom</translation>
    </message>
</context>
<context>
    <name>Toolbar3</name>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="56"/>
        <source>File format</source>
        <translation>Format de fitxer</translation>
    </message>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="168"/>
        <source>Mark area and click save again</source>
        <translation>Marqueu l&apos;àrea i cliqueu &apos;desa&apos; un altre cop</translation>
    </message>
    <message>
        <source>Save failed...</source>
        <translation type="vanished">No s&apos;ha pogut desar</translation>
    </message>
</context>
<context>
    <name>Toolbox</name>
    <message>
        <location filename="../qml/components/Toolbox.qml" line="51"/>
        <source>Clearing</source>
        <translation>Netejant</translation>
    </message>
</context>
<context>
    <name>askFilenameDialog</name>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="49"/>
        <location filename="../qml/pages/askFilenameDialog.qml" line="57"/>
        <source>Enter filename</source>
        <translation>Introduïu el nom del fitxer</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="75"/>
        <source>File already exists</source>
        <translation>El fitxer ja existeix</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="84"/>
        <source>Crop before saving</source>
        <translation>Retalleu abans de desar</translation>
    </message>
</context>
<context>
    <name>bgSettingsDialog</name>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="46"/>
        <source>Select background</source>
        <translation>Selecciona un fons</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="65"/>
        <source>Select color</source>
        <translation>Seleccioneu el color</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="78"/>
        <source>None</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="100"/>
        <source>Image</source>
        <translation>Imatge</translation>
    </message>
</context>
<context>
    <name>dimensionDialog</name>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="37"/>
        <source>Warning: Do not cancel now</source>
        <translation>Alerta: No cancel·lis ara</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="53"/>
        <source>Dimensioning</source>
        <translation>Dimensionament</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="71"/>
        <source>Scale all dimensions</source>
        <translation>Escala totes les dimensions</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="77"/>
        <source>Reference length %1</source>
        <translation>Llargada de referència %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="83"/>
        <source>Currently scaled length %1</source>
        <translation>Llargada escalada actual %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="91"/>
        <source>Enter true length</source>
        <translation>Introduïu la llargada real</translation>
    </message>
</context>
<context>
    <name>eraserSettingsDialog</name>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="37"/>
        <source>Eraser settings</source>
        <translation>Configuració de la goma d&apos;esborrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="55"/>
        <source>Eraser size</source>
        <translation>Mida de la goma d&apos;esborrar</translation>
    </message>
</context>
<context>
    <name>genSettings</name>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="42"/>
        <source>General settings</source>
        <translation>Configuració general</translation>
    </message>
    <message>
        <source>File format</source>
        <translation type="vanished">Format de fitxer</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="62"/>
        <source>Saving options</source>
        <translation>Opcions de desament</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="100"/>
        <source>Ask filename when saving</source>
        <translation>Pregunta el nom del fitxer en desar</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="108"/>
        <source>Toolbox location</source>
        <translation>Ubicació de les eines</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="118"/>
        <source>Top</source>
        <translation>Capçalera</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="132"/>
        <source>Bottom</source>
        <translation>Peu</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="146"/>
        <source>Grid settings</source>
        <translation>Configuració de quadrícula</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="153"/>
        <source>Snap to Grid</source>
        <translation>Ajusta a la quadrícula</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="161"/>
        <source>Grid spacing</source>
        <translation>Espaiat de la quadrícula</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="172"/>
        <source>Tool settings</source>
        <translation>Configuració de les eines</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="179"/>
        <source>Remember tool settings</source>
        <translation>Recorda la configuració de les eines</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="186"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="192"/>
        <source>Childs play mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="193"/>
        <source>Disables system gestures. Press powerkey to minimize paint.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>layersDialog</name>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="23"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="29"/>
        <source>Add new layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="35"/>
        <source>Current layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="40"/>
        <source>Active layer is always shown on top when editing. Layers are saved in order shown here. Only visible layers are saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="113"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>paint</name>
    <message>
        <location filename="../qml/paint.qml" line="13"/>
        <source>Accept</source>
        <translation>Accepta</translation>
    </message>
    <message>
        <location filename="../qml/paint.qml" line="14"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
</context>
<context>
    <name>penSettingsDialog</name>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="44"/>
        <source>Pen settings</source>
        <translation>Configuració de la ploma</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="62"/>
        <source>Select color</source>
        <translation>Seleccioneu el color</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="80"/>
        <source>Pen width</source>
        <translation>Mida de la ploma</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="146"/>
        <source>Brush style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>shaderSettingsDialog</name>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="21"/>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="26"/>
        <source>Shaders have fragment shader and optional vertex shader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="108"/>
        <source>Shader log output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="121"/>
        <source>Select a shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="166"/>
        <source>FS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="169"/>
        <source>VS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>spraySettingsDialog</name>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="61"/>
        <source>Sprayer settings</source>
        <translation>Configuració de l&apos;esprai</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="80"/>
        <source>Select color</source>
        <translation>Seleccioneu el color</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="126"/>
        <source>Sprayer parameters</source>
        <translation>Paràmetres de l&apos;esprai</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="132"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="145"/>
        <source>Density</source>
        <translation>Densitat</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="159"/>
        <source>Particle size</source>
        <translation>Mida de les partícules</translation>
    </message>
</context>
<context>
    <name>textEntryDialog</name>
    <message>
        <location filename="../qml/pages/textEntryDialog.qml" line="51"/>
        <source>Enter some text</source>
        <translation>Introduïu algun text</translation>
    </message>
</context>
<context>
    <name>textSettingsDialog</name>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="42"/>
        <source>Text settings</source>
        <translation>Configuració del text</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="60"/>
        <source>Select color</source>
        <translation>Seleccioneu el color</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="71"/>
        <source>Font size</source>
        <translation>Mida de font</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="93"/>
        <source>Bold</source>
        <translation>Negreta</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="100"/>
        <source>Italic</source>
        <translation>Itàlica</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="108"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
</context>
</TS>
