<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb">
<context>
    <name>AboutPage</name>
    <message>
        <source>Change toolbar</source>
        <translation type="vanished">Bytt verktøylinje</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="29"/>
        <source>Draw freehand line</source>
        <translation>Frihåndstegning</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="30"/>
        <source>Eraser</source>
        <translation>Viskelær</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Sprayer</source>
        <translation>Sprayboks</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>Draw geometric shape</source>
        <translation>Geometrisk form</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>Change color, width</source>
        <translation>Endre farge, bredde</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Geometrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Draw line</source>
        <translation>Linje</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>Draw rectangle</source>
        <translation>Rektangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Draw filled rectangle</source>
        <translation>Fylt rektangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Draw square</source>
        <translation>Kvadrat</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <source>Draw filled square</source>
        <translation>Fylt kvadrat</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Draw circle</source>
        <translation>Sirkel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>Draw filled circle</source>
        <translation>Fylt sirkel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>Draw ellipse</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>Draw filled ellipse</source>
        <translation>Fylt ellipse</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>Draw equilateral triangle</source>
        <translation>Likesidet trekant</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>Draw filled e-triangle</source>
        <translation>Fylt likesidet trekant</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Draw right isosceles triangle</source>
        <translation>Høyrevendt rettvinklet likebeint trekant</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Draw filled ri-triangle</source>
        <translation>Fylt høyrevendt rettvinklet likebeint trekant</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>Draw regular polygon</source>
        <translation>Likesidet polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Draw filled regular polygon</source>
        <translation>Fylt likesidet polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Draw regular polygram</source>
        <translation>Likesidet polygram</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>Draw filled regular polygram</source>
        <translation>Fylt likesidet polygram</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>More tools toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Add/cancel text</source>
        <translation>Legg til/slett tekst</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Add/cancel image</source>
        <translation>Legg til/slett bilde</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Draw thick arrow</source>
        <translation>Bred pil</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>Drawing toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Draw filled thick arrow</source>
        <translation>Fylt bred pil</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Fill mode toggle</source>
        <translation>Slå av/på fyllmodus</translation>
    </message>
    <message>
        <source>Text tool (cancel text)</source>
        <translation type="vanished">Tekstverktøy (fjern tekst)</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Accept current text/image</source>
        <translation>Bruk tekst/bilde</translation>
    </message>
    <message>
        <source>Accept current text</source>
        <translation type="vanished">Bruk tekst</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Dimensioning tool</source>
        <translation>Skaleringsverktøy</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>Grid</source>
        <translation>Rutenett</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>File toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>About Paint</source>
        <translation>Om Male</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>Change settings</source>
        <translation>Endre innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="64"/>
        <source>Clear drawing</source>
        <translation>Fjern alt</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="65"/>
        <source>Change background</source>
        <translation>Endre bakgrunn</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Save snapshot</source>
        <translation>Lagre øyeblikksbilde</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>Clipboard and layers toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="69"/>
        <source>Paste from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="70"/>
        <source>Accept current paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="71"/>
        <source>Layer down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>Layer up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>Configure layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>Shader toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="75"/>
        <source>Shader mode enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="76"/>
        <source>Accept current shading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Apply shader fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="78"/>
        <source>Show shader popup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>Configure shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <source>About </source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="137"/>
        <source>Version: </source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="146"/>
        <source>translation credit placeholder</source>
        <translation>Kjetil Kilhavn</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="156"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
</context>
<context>
    <name>ColorSelector</name>
    <message>
        <location filename="../qml/components/ColorSelector.qml" line="27"/>
        <source>Edit color %1</source>
        <translation>Rediger farge %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="23"/>
        <source>Paint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaSelector</name>
    <message>
        <source>Image selected</source>
        <translation type="vanished">Bilde valgt</translation>
    </message>
    <message>
        <source>Select image</source>
        <translation type="vanished">Velg bilde</translation>
    </message>
</context>
<context>
    <name>Paint</name>
    <message>
        <location filename="../qml/pages/Paint.qml" line="48"/>
        <source>Save failed...</source>
        <translation type="unfinished">Lagring feilet...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="277"/>
        <source>Swipe to change toolbar</source>
        <translation>Sveip for å bytte verktøylinje</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="297"/>
        <source>Pinch to zoom</source>
        <translation>Knip for å forstørre/forminske</translation>
    </message>
</context>
<context>
    <name>Toolbar3</name>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="56"/>
        <source>File format</source>
        <translation>Filformat</translation>
    </message>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="168"/>
        <source>Mark area and click save again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save failed...</source>
        <translation type="vanished">Lagring feilet...</translation>
    </message>
</context>
<context>
    <name>Toolbox</name>
    <message>
        <location filename="../qml/components/Toolbox.qml" line="51"/>
        <source>Clearing</source>
        <translation>Fjerner</translation>
    </message>
</context>
<context>
    <name>askFilenameDialog</name>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="49"/>
        <location filename="../qml/pages/askFilenameDialog.qml" line="57"/>
        <source>Enter filename</source>
        <translation>Angi filnavn</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="75"/>
        <source>File already exists</source>
        <translation>Fila eksisterer allerede</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="84"/>
        <source>Crop before saving</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>bgSettingsDialog</name>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="46"/>
        <source>Select background</source>
        <translation>Velg bakgrunn</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="65"/>
        <source>Select color</source>
        <translation>Velg farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="78"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="100"/>
        <source>Image</source>
        <translation>Bilde</translation>
    </message>
</context>
<context>
    <name>dimensionDialog</name>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="37"/>
        <source>Warning: Do not cancel now</source>
        <translation>Advarsel: Ikke avbryt nå</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="53"/>
        <source>Dimensioning</source>
        <translation>Skalerer</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="71"/>
        <source>Scale all dimensions</source>
        <translation>Skaler alle dimensjoner</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="77"/>
        <source>Reference length %1</source>
        <translation>Referanselengde %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="83"/>
        <source>Currently scaled length %1</source>
        <translation>Nåværende skalert lengde %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="91"/>
        <source>Enter true length</source>
        <translation>Angi faktisk lengde</translation>
    </message>
</context>
<context>
    <name>eraserSettingsDialog</name>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="37"/>
        <source>Eraser settings</source>
        <translation>Innstillinger for viskelær</translation>
    </message>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="55"/>
        <source>Eraser size</source>
        <translation>Størrelse</translation>
    </message>
</context>
<context>
    <name>genSettings</name>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="42"/>
        <source>General settings</source>
        <translation>Generelle innstillinger</translation>
    </message>
    <message>
        <source>File format</source>
        <translation type="vanished">Filformat</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="62"/>
        <source>Saving options</source>
        <translation>Valg for lagring</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="100"/>
        <source>Ask filename when saving</source>
        <translation>Spør om filnavn ved lagring</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="108"/>
        <source>Toolbox location</source>
        <translation>Plassering av verktøylinje</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="118"/>
        <source>Top</source>
        <translation>Øverst</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="132"/>
        <source>Bottom</source>
        <translation>Nederst</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="146"/>
        <source>Grid settings</source>
        <translation>Innstillinger for rutenett</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="153"/>
        <source>Snap to Grid</source>
        <translation>Fest til rutenett</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="161"/>
        <source>Grid spacing</source>
        <translation>Rutestørrelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="172"/>
        <source>Tool settings</source>
        <translation>Innstillinger for verktøy</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="179"/>
        <source>Remember tool settings</source>
        <translation>Husk innstillinger for verktøy</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="186"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="192"/>
        <source>Childs play mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="193"/>
        <source>Disables system gestures. Press powerkey to minimize paint.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>layersDialog</name>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="23"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="29"/>
        <source>Add new layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="35"/>
        <source>Current layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="40"/>
        <source>Active layer is always shown on top when editing. Layers are saved in order shown here. Only visible layers are saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="113"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>paint</name>
    <message>
        <location filename="../qml/paint.qml" line="13"/>
        <source>Accept</source>
        <translation>Godta</translation>
    </message>
    <message>
        <location filename="../qml/paint.qml" line="14"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>penSettingsDialog</name>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="44"/>
        <source>Pen settings</source>
        <translation>Innstillinger for penn</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="62"/>
        <source>Select color</source>
        <translation>Velg farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="80"/>
        <source>Pen width</source>
        <translation>Pennbredde</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="146"/>
        <source>Brush style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>shaderSettingsDialog</name>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="21"/>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="26"/>
        <source>Shaders have fragment shader and optional vertex shader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="108"/>
        <source>Shader log output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="121"/>
        <source>Select a shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="166"/>
        <source>FS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="169"/>
        <source>VS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>spraySettingsDialog</name>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="61"/>
        <source>Sprayer settings</source>
        <translation>Innstillinger for sprayboks</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="80"/>
        <source>Select color</source>
        <translation>Velg farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="126"/>
        <source>Sprayer parameters</source>
        <translation>Parametere for sprayboks</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="132"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="145"/>
        <source>Density</source>
        <translation>Tetthet</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="159"/>
        <source>Particle size</source>
        <translation>Partikkelstørrelse</translation>
    </message>
</context>
<context>
    <name>textEntryDialog</name>
    <message>
        <location filename="../qml/pages/textEntryDialog.qml" line="51"/>
        <source>Enter some text</source>
        <translation>Skriv inn tekst</translation>
    </message>
</context>
<context>
    <name>textSettingsDialog</name>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="42"/>
        <source>Text settings</source>
        <translation>Innstillinger for tekst</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="60"/>
        <source>Select color</source>
        <translation>Velg farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="71"/>
        <source>Font size</source>
        <translation>Skriftstørrelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="93"/>
        <source>Bold</source>
        <translation>Fet skrift</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="100"/>
        <source>Italic</source>
        <translation>Kursivskrift</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="108"/>
        <source>Font</source>
        <translation>Skrift</translation>
    </message>
</context>
</TS>
