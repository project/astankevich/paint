<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutPage</name>
    <message>
        <source>Change toolbar</source>
        <translation type="vanished">Переключить панель</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="29"/>
        <source>Draw freehand line</source>
        <translation>Карандаш</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="30"/>
        <source>Eraser</source>
        <translation>Ластик</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Sprayer</source>
        <translation>Распылитель</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>Draw geometric shape</source>
        <translation>Простые формы</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>Change color, width</source>
        <translation>Цвет линии, размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Geometrics</source>
        <translation>Геометрия</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Draw line</source>
        <translation>Линия</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>Draw rectangle</source>
        <translation>Прямоугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Draw filled rectangle</source>
        <translation>Залитый прямоугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Draw square</source>
        <translation>Квадрат</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <source>Draw filled square</source>
        <translation>Залитый квадрат</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Draw circle</source>
        <translation>Круг</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>Draw filled circle</source>
        <translation>Залитый круг</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>Draw ellipse</source>
        <translation>Эллипс</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>Draw filled ellipse</source>
        <translation>Залитый эллипс</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>Draw equilateral triangle</source>
        <translation>Равносторонний треугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>Draw filled e-triangle</source>
        <translation>Залитый равностор. треугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Draw right isosceles triangle</source>
        <translation>Прямоугольный треугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Draw filled ri-triangle</source>
        <translation>Залитый прямоуг. треугольник</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>Draw regular polygon</source>
        <translation>Полигон</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Draw filled regular polygon</source>
        <translation>Залитый полигон</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Draw regular polygram</source>
        <translation>Полиграм</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>Draw filled regular polygram</source>
        <translation>Залитый полиграм</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>More tools toolbar</source>
        <translation>Дополнительный набор инструментов</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Add/cancel text</source>
        <translation>Добавить/убрать текст</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Add/cancel image</source>
        <translation>Добавить/убрать изображение</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Draw thick arrow</source>
        <translation>Стрелка</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>Drawing toolbar</source>
        <translation>Набор инструментов для рисования</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Draw filled thick arrow</source>
        <translation>Залитая стрелка</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Fill mode toggle</source>
        <translation>Заливка</translation>
    </message>
    <message>
        <source>Text tool (cancel text)</source>
        <translation type="vanished">Вставка (отмена) текста</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Accept current text/image</source>
        <translation>Подтвердить текст/изображение</translation>
    </message>
    <message>
        <source>Accept current text</source>
        <translation type="vanished">Подтвердить текст</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Dimensioning tool</source>
        <translation>Линейка</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>Grid</source>
        <translation>Сетка</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>File toolbar</source>
        <translation>Набор инструментов для работы с файлом</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>About Paint</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>Change settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="64"/>
        <source>Clear drawing</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="65"/>
        <source>Change background</source>
        <translation>Изменить фон</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Save snapshot</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>Clipboard and layers toolbar</source>
        <translation>Набор инструментов для буфера обмена и слоёв</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>Clipboard</source>
        <translation>Буфер обмена</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="69"/>
        <source>Paste from clipboard</source>
        <translation>Вставить из буфера обмена</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="70"/>
        <source>Accept current paste</source>
        <translation>Принять текущию вставку</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="71"/>
        <source>Layer down</source>
        <translation>Переместить слой вниз</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>Layer up</source>
        <translation>Переместить слой наверх</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>Configure layers</source>
        <translation>Настроить слои</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>Shader toolbar</source>
        <translation>Набор инструментов шейдеров</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="75"/>
        <source>Shader mode enable</source>
        <translation>Включить режим шейдеров</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="76"/>
        <source>Accept current shading</source>
        <translation>Применить текущие шейдеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Apply shader fullscreen</source>
        <translation>Применить шейдер в полноэкранном режиме</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="78"/>
        <source>Show shader popup</source>
        <translation>Показать всплывающее окно шейдера</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>Configure shaders</source>
        <translation>Настроить шейдеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <source>About </source>
        <translation>О программе </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="137"/>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="146"/>
        <source>translation credit placeholder</source>
        <translation>Переведено и озвучено кем-то с ником coderus</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="156"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
</context>
<context>
    <name>ColorSelector</name>
    <message>
        <location filename="../qml/components/ColorSelector.qml" line="27"/>
        <source>Edit color %1</source>
        <translation>Изменить цвет %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="23"/>
        <source>Paint</source>
        <translation>Рисовать</translation>
    </message>
</context>
<context>
    <name>MediaSelector</name>
    <message>
        <source>Image selected</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <source>Select image</source>
        <translation type="vanished">Выберите</translation>
    </message>
</context>
<context>
    <name>Paint</name>
    <message>
        <location filename="../qml/pages/Paint.qml" line="48"/>
        <source>Save failed...</source>
        <translation>Сохранение не удалось...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="277"/>
        <source>Swipe to change toolbar</source>
        <translation>Проведите, чтобы сменить панель</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="297"/>
        <source>Pinch to zoom</source>
        <translation>Ущипнуть, чтобы изменить масштаб</translation>
    </message>
</context>
<context>
    <name>Toolbar3</name>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="56"/>
        <source>File format</source>
        <translation>Формат файла</translation>
    </message>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="168"/>
        <source>Mark area and click save again</source>
        <translation>Выделите область инашмите Сохранить ещё раз</translation>
    </message>
    <message>
        <source>Save failed...</source>
        <translation type="vanished">Сохранение не удалось...</translation>
    </message>
</context>
<context>
    <name>Toolbox</name>
    <message>
        <location filename="../qml/components/Toolbox.qml" line="51"/>
        <source>Clearing</source>
        <translation>Очистка</translation>
    </message>
</context>
<context>
    <name>askFilenameDialog</name>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="49"/>
        <location filename="../qml/pages/askFilenameDialog.qml" line="57"/>
        <source>Enter filename</source>
        <translation>Введите имя файла</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="75"/>
        <source>File already exists</source>
        <translation>Файл уже существует</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="84"/>
        <source>Crop before saving</source>
        <translation>Обрезать перед сохранением</translation>
    </message>
</context>
<context>
    <name>bgSettingsDialog</name>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="46"/>
        <source>Select background</source>
        <translation>Выберите фон</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="65"/>
        <source>Select color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="78"/>
        <source>None</source>
        <translation>Ничего</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="100"/>
        <source>Image</source>
        <translation>Изображение</translation>
    </message>
</context>
<context>
    <name>dimensionDialog</name>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="37"/>
        <source>Warning: Do not cancel now</source>
        <translation>Внимание: не отменяйте сейчас</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="53"/>
        <source>Dimensioning</source>
        <translation>Размеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="71"/>
        <source>Scale all dimensions</source>
        <translation>Масштабирование</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="77"/>
        <source>Reference length %1</source>
        <translation>Фактический размер: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="83"/>
        <source>Currently scaled length %1</source>
        <translation>Текущий размер: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="91"/>
        <source>Enter true length</source>
        <translation>Введите нужный размер</translation>
    </message>
</context>
<context>
    <name>eraserSettingsDialog</name>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="37"/>
        <source>Eraser settings</source>
        <translation>Ластик</translation>
    </message>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="55"/>
        <source>Eraser size</source>
        <translation>Размер</translation>
    </message>
</context>
<context>
    <name>genSettings</name>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="42"/>
        <source>General settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>File format</source>
        <translation type="vanished">Формат файла</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="62"/>
        <source>Saving options</source>
        <translation>Настройки сохраниения</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="100"/>
        <source>Ask filename when saving</source>
        <translation>Спросить имя файла перед сохранением</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="108"/>
        <source>Toolbox location</source>
        <translation>Положение панели</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="118"/>
        <source>Top</source>
        <translation>Сверху</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="132"/>
        <source>Bottom</source>
        <translation>Снизу</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="146"/>
        <source>Grid settings</source>
        <translation>Сетка</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="153"/>
        <source>Snap to Grid</source>
        <translation>Привязка к сетке</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="161"/>
        <source>Grid spacing</source>
        <translation>Шаг сетки</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="172"/>
        <source>Tool settings</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="179"/>
        <source>Remember tool settings</source>
        <translation>Запомнить настройки</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="186"/>
        <source>Other</source>
        <translation>Другие</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="192"/>
        <source>Childs play mode</source>
        <translation>Игровой режим для детей</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="193"/>
        <source>Disables system gestures. Press powerkey to minimize paint.</source>
        <translation>Выключить системный жесты. Нажмите кнопку питания чтобы свернуть Рисовать.</translation>
    </message>
</context>
<context>
    <name>layersDialog</name>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="23"/>
        <source>Layers</source>
        <translation>Слои</translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="29"/>
        <source>Add new layer</source>
        <translation>Добавить новый слой</translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="35"/>
        <source>Current layers</source>
        <translation>Текущие слои</translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="40"/>
        <source>Active layer is always shown on top when editing. Layers are saved in order shown here. Only visible layers are saved.</source>
        <translation>Активнй слой всегда показывается сверху. Слои сохраняются в том же порядке в каком показаны. Сохраняются только выидимые слои.</translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="113"/>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
</context>
<context>
    <name>paint</name>
    <message>
        <location filename="../qml/paint.qml" line="13"/>
        <source>Accept</source>
        <translation>Подтвердить</translation>
    </message>
    <message>
        <location filename="../qml/paint.qml" line="14"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>penSettingsDialog</name>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="44"/>
        <source>Pen settings</source>
        <translation>Карандаш</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="62"/>
        <source>Select color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="80"/>
        <source>Pen width</source>
        <translation>Толщина</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="146"/>
        <source>Brush style</source>
        <translation>Стиль кисти</translation>
    </message>
</context>
<context>
    <name>shaderSettingsDialog</name>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="21"/>
        <source>Shaders</source>
        <translation>Шейдеры</translation>
    </message>
    <message>
        <source>Shaders have fragment shader and optional vertex shader. blaa.</source>
        <translation type="vanished">Шейдеры имеют фрагнемный шейдер и необязательный вертексный. Блаа.</translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="26"/>
        <source>Shaders have fragment shader and optional vertex shader.</source>
        <translation>Шейдеры имеют фрагментный шейдер и необязательный вертексный.</translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="108"/>
        <source>Shader log output</source>
        <translation>Вывод шейдерного лога</translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="121"/>
        <source>Select a shader</source>
        <translation>Выдерите шейдер</translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="166"/>
        <source>FS</source>
        <translation>ФШ</translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="169"/>
        <source>VS</source>
        <translation>ВШ</translation>
    </message>
</context>
<context>
    <name>spraySettingsDialog</name>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="61"/>
        <source>Sprayer settings</source>
        <translation>Распылитель</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="80"/>
        <source>Select color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="126"/>
        <source>Sprayer parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="132"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="145"/>
        <source>Density</source>
        <translation>Плотность</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="159"/>
        <source>Particle size</source>
        <translation>Размер частиц</translation>
    </message>
</context>
<context>
    <name>textEntryDialog</name>
    <message>
        <location filename="../qml/pages/textEntryDialog.qml" line="51"/>
        <source>Enter some text</source>
        <translation>Введите текст</translation>
    </message>
</context>
<context>
    <name>textSettingsDialog</name>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="42"/>
        <source>Text settings</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="60"/>
        <source>Select color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="71"/>
        <source>Font size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="93"/>
        <source>Bold</source>
        <translation>Жирный</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="100"/>
        <source>Italic</source>
        <translation>Курсивный</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="108"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
</context>
</TS>
