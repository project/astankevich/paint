<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <source>Change toolbar</source>
        <translation type="vanished">Byt verktygsfält</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="29"/>
        <source>Draw freehand line</source>
        <translation>Rita frihandslinje</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="30"/>
        <source>Eraser</source>
        <translation>Radera</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="31"/>
        <source>Sprayer</source>
        <translation>Färgspruta</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="32"/>
        <source>Draw geometric shape</source>
        <translation>Rita geometrisk figur</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>Change color, width</source>
        <translation>Ändra färg och tjocklek</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Geometrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Draw line</source>
        <translation>Rita linje</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>Draw rectangle</source>
        <translation>Rita rektangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Draw filled rectangle</source>
        <translation>Rita fylld rektangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Draw square</source>
        <translation>Rita kvadrat</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <source>Draw filled square</source>
        <translation>Rita fylld kvadrat</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Draw circle</source>
        <translation>Rita cirkel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>Draw filled circle</source>
        <translation>Rita fylld cirkel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>Draw ellipse</source>
        <translation>Rita ellips</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>Draw filled ellipse</source>
        <translation>Rita fylld ellips</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>Draw equilateral triangle</source>
        <translation>Rita liksidig triangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>Draw filled e-triangle</source>
        <translation>Rita fylld l-rektangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Draw right isosceles triangle</source>
        <translation>Rita likbent triangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Draw filled ri-triangle</source>
        <translation>Rita fylld l-triangel</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>Draw regular polygon</source>
        <translation>Rita regelbunden polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Draw filled regular polygon</source>
        <translation>Rita fylld regelbunden polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Draw regular polygram</source>
        <translation>Rita regelbunden polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>Draw filled regular polygram</source>
        <translation>Rita fylld regelbunden polygon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>More tools toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Add/cancel text</source>
        <translation>Lägg till/Avbryt text</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Add/cancel image</source>
        <translation>Lägg till/Avbryt bild</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Draw thick arrow</source>
        <translation>Rita tjock pil</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="28"/>
        <source>Drawing toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Draw filled thick arrow</source>
        <translation>Rita fylld tjock pil</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="54"/>
        <source>Fill mode toggle</source>
        <translation>Växla fyllnad/transparens</translation>
    </message>
    <message>
        <source>Text tool (cancel text)</source>
        <translation type="vanished">Textverktyg (avbryt text)</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Accept current text/image</source>
        <translation>Acceptera aktuell text/bild</translation>
    </message>
    <message>
        <source>Accept current text</source>
        <translation type="vanished">Acceptera aktuell text</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Dimensioning tool</source>
        <translation>Dimensioneringsverktyg</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>Grid</source>
        <translation>Stödlinjer</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>File toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="62"/>
        <source>About Paint</source>
        <translation>Om Paint</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="63"/>
        <source>Change settings</source>
        <translation>Ändra inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="64"/>
        <source>Clear drawing</source>
        <translation>Rensa skärmen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="65"/>
        <source>Change background</source>
        <translation>Ändra bakgrund</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="66"/>
        <source>Save snapshot</source>
        <translation>Spara ögonblicksbild</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>Clipboard and layers toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="69"/>
        <source>Paste from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="70"/>
        <source>Accept current paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="71"/>
        <source>Layer down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>Layer up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>Configure layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="74"/>
        <source>Shader toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="75"/>
        <source>Shader mode enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="76"/>
        <source>Accept current shading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>Apply shader fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="78"/>
        <source>Show shader popup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>Configure shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="137"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="146"/>
        <source>translation credit placeholder</source>
        <translation>Svensk översättning av Åke Engelbrektson</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="156"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
</context>
<context>
    <name>ColorSelector</name>
    <message>
        <location filename="../qml/components/ColorSelector.qml" line="27"/>
        <source>Edit color %1</source>
        <translation>Ändra färg %1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="23"/>
        <source>Paint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaSelector</name>
    <message>
        <source>Image selected</source>
        <translation type="vanished">Bild vald</translation>
    </message>
    <message>
        <source>Select image</source>
        <translation type="vanished">Välj bild</translation>
    </message>
</context>
<context>
    <name>Paint</name>
    <message>
        <location filename="../qml/pages/Paint.qml" line="48"/>
        <source>Save failed...</source>
        <translation type="unfinished">Kunde inte spara...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="277"/>
        <source>Swipe to change toolbar</source>
        <translation>Svep för att byta verktygsfält</translation>
    </message>
    <message>
        <location filename="../qml/pages/Paint.qml" line="297"/>
        <source>Pinch to zoom</source>
        <translation>Nyp för att zooma</translation>
    </message>
</context>
<context>
    <name>Toolbar3</name>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="56"/>
        <source>File format</source>
        <translation>Filformat</translation>
    </message>
    <message>
        <location filename="../qml/components/Toolbar3.qml" line="168"/>
        <source>Mark area and click save again</source>
        <translation>Markera område och tryck spara igen</translation>
    </message>
    <message>
        <source>Save failed...</source>
        <translation type="vanished">Kunde inte spara...</translation>
    </message>
</context>
<context>
    <name>Toolbox</name>
    <message>
        <location filename="../qml/components/Toolbox.qml" line="51"/>
        <source>Clearing</source>
        <translation>Rensar</translation>
    </message>
</context>
<context>
    <name>askFilenameDialog</name>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="49"/>
        <location filename="../qml/pages/askFilenameDialog.qml" line="57"/>
        <source>Enter filename</source>
        <translation>Ange filnamn</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="75"/>
        <source>File already exists</source>
        <translation>Filen finns redan</translation>
    </message>
    <message>
        <location filename="../qml/pages/askFilenameDialog.qml" line="84"/>
        <source>Crop before saving</source>
        <translation>Beskär före spara</translation>
    </message>
</context>
<context>
    <name>bgSettingsDialog</name>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="46"/>
        <source>Select background</source>
        <translation>Välj bakgrund</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="65"/>
        <source>Select color</source>
        <translation>Välj färg</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="78"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../qml/pages/bgSettingsDialog.qml" line="100"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
</context>
<context>
    <name>dimensionDialog</name>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="37"/>
        <source>Warning: Do not cancel now</source>
        <translation>Varning! Avbryt inte nu</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="53"/>
        <source>Dimensioning</source>
        <translation>Dimensionering</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="71"/>
        <source>Scale all dimensions</source>
        <translation>Skala samtliga dimensioner</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="77"/>
        <source>Reference length %1</source>
        <translation>Referenslängd %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="83"/>
        <source>Currently scaled length %1</source>
        <translation>Aktuell skalad längd %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/dimensionDialog.qml" line="91"/>
        <source>Enter true length</source>
        <translation>Ange verklig längd</translation>
    </message>
</context>
<context>
    <name>eraserSettingsDialog</name>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="37"/>
        <source>Eraser settings</source>
        <translation>Raderingsinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/eraserSettingsDialog.qml" line="55"/>
        <source>Eraser size</source>
        <translation>Raderingsbredd</translation>
    </message>
</context>
<context>
    <name>genSettings</name>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="42"/>
        <source>General settings</source>
        <translation>Allmäna inställningar</translation>
    </message>
    <message>
        <source>File format</source>
        <translation type="vanished">Filformat</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="62"/>
        <source>Saving options</source>
        <translation>Spara-alternativ</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="100"/>
        <source>Ask filename when saving</source>
        <translation>Fråga efter filnamn vid Spara</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="108"/>
        <source>Toolbox location</source>
        <translation>Verktygsplacering</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="118"/>
        <source>Top</source>
        <translation>I överkant</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="132"/>
        <source>Bottom</source>
        <translation>I underkant</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="146"/>
        <source>Grid settings</source>
        <translation>Stödlinjeinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="153"/>
        <source>Snap to Grid</source>
        <translation>Fäst vid stödlinjer</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="161"/>
        <source>Grid spacing</source>
        <translation>Stödlinjeavstånd</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="172"/>
        <source>Tool settings</source>
        <translation>Verktygsinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="179"/>
        <source>Remember tool settings</source>
        <translation>Kom ihåg verktygsinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="186"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="192"/>
        <source>Childs play mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/genSettings.qml" line="193"/>
        <source>Disables system gestures. Press powerkey to minimize paint.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>layersDialog</name>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="23"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="29"/>
        <source>Add new layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="35"/>
        <source>Current layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="40"/>
        <source>Active layer is always shown on top when editing. Layers are saved in order shown here. Only visible layers are saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/layersDialog.qml" line="113"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>paint</name>
    <message>
        <location filename="../qml/paint.qml" line="13"/>
        <source>Accept</source>
        <translation>Verkställ</translation>
    </message>
    <message>
        <location filename="../qml/paint.qml" line="14"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>penSettingsDialog</name>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="44"/>
        <source>Pen settings</source>
        <translation>Penninställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="62"/>
        <source>Select color</source>
        <translation>Välj färg</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="80"/>
        <source>Pen width</source>
        <translation>Penntjocklek</translation>
    </message>
    <message>
        <location filename="../qml/pages/penSettingsDialog.qml" line="146"/>
        <source>Brush style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>shaderSettingsDialog</name>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="21"/>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="26"/>
        <source>Shaders have fragment shader and optional vertex shader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="108"/>
        <source>Shader log output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="121"/>
        <source>Select a shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="166"/>
        <source>FS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/shaderSettingsDialog.qml" line="169"/>
        <source>VS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>spraySettingsDialog</name>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="61"/>
        <source>Sprayer settings</source>
        <translation>Spray-inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="80"/>
        <source>Select color</source>
        <translation>Välj färg</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="126"/>
        <source>Sprayer parameters</source>
        <translation>Spray-parametrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="132"/>
        <source>Size</source>
        <translation>Storlek</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="145"/>
        <source>Density</source>
        <translation>Densitet</translation>
    </message>
    <message>
        <location filename="../qml/pages/spraySettingsDialog.qml" line="159"/>
        <source>Particle size</source>
        <translation>Partikelstorlek</translation>
    </message>
</context>
<context>
    <name>textEntryDialog</name>
    <message>
        <location filename="../qml/pages/textEntryDialog.qml" line="51"/>
        <source>Enter some text</source>
        <translation>Mata in text</translation>
    </message>
</context>
<context>
    <name>textSettingsDialog</name>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="42"/>
        <source>Text settings</source>
        <translation>Textinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="60"/>
        <source>Select color</source>
        <translation>Välj färg</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="71"/>
        <source>Font size</source>
        <translation>Teckenstorlek</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="93"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="100"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/textSettingsDialog.qml" line="108"/>
        <source>Font</source>
        <translation>Teckensnitt</translation>
    </message>
</context>
</TS>
